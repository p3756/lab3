import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        int n,a ;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number : ");
        n = sc.nextInt();
        sc.close();
        if(n == 1){
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            a = sc.nextInt();
            for(int i=0; i<a; i++){
                for(int j=0; j<i+1; j++){
                    System.out.print("*");
                }
                System.out.println();
            }    
        }else if(n == 2){
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            a = sc.nextInt();
            for(int i = a; i>0 ; i--) {
                for(int j=0; j<i; j++){
                    System.out.print("*");
                }
                System.out.println();
            }    
        }else if(n==3){
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            a = sc.nextInt();
            for(int i = 0; i<a ; i++) {
                for(int j=0; j<i; j++){
                    System.out.print(" ");
                }
                for(int j = 0; j <5 -i; j++){
                    System.out.print("*");
                }
                System.out.println();
            }    
        }else if(n==4){
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            a = sc.nextInt();
            for(int i=4; i>=0; i--){
                for(int j = 0; j<a; j++){
                    if(j>=i){
                        System.out.print("*");
                    }else{
                        System.out.print(" ");
                    }
                    
                }System.out.println();
            }   
        }else if(n==5){
            System.out.println("Bye bye");
        }else{
            System.out.println("Error: Please input number between 1-5");
        }
    }   
}      
